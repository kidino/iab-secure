<div class="col-md-6">
<form method="POST" action="{{ route('user-profile-information.update') }}" 
class="p-4 mb-4 shadow rounded">
    @csrf
    @method('PUT')

    <h5>Update Your Details</h5>

    <div class="mt-3">
        <label>{{ __('Name') }}</label>
        <input class="form-control" type="text" name="name" value="{{ old('name') ?? auth()->user()->name }}" required autofocus autocomplete="name" />
    </div>

    <div class="mt-3">
        <label>{{ __('Email') }}</label>
        <input class="form-control" type="email" name="email" value="{{ old('email') ?? auth()->user()->email }}" required autofocus />
    </div>

    <div class="mt-3">
        <button class="btn btn-primary" type="submit">
            {{ __('Update Profile') }}
        </button>
    </div>
</form>
</div>