<div class="col-md-6">
<form method="POST" action="{{ route('user-password.update') }}" 
class="p-4 mb-4 shadow rounded">
    @csrf
    @method('PUT')

    <h5>Update Your Password</h5>

    <div class="mt-3">
        <label>{{ __('Current Password') }}</label>
        <input class="form-control" type="password" name="current_password" required autocomplete="current-password" />
    </div>

    <div class="mt-3">
        <label>{{ __('Password') }}</label>
        <input class="form-control" type="password" name="password" required autocomplete="new-password" />
    </div>

    <div class="mt-3">
        <label>{{ __('Confirm Password') }}</label>
        <input class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
    </div>

    <div class="mt-3">
        <button class="btn btn-primary" type="submit">
            {{ __('Update Password') }}
        </button>
    </div>
</form>
</div>